lstEng = []
dctEng = dict()

lstKa = []
dctKa = dict()

lstRu = []
dctRu = dict()


def findKey(text):
  import re
  matches=re.findall(r'\"(.+?)\"',text)
  # matches is now ['String 1', 'String 2', 'String3']
  return ",".join(matches)

def findValue(text):
  import re
  matches=re.findall(r'\>(.+?)\</string>',text)
  return ",".join(matches)

"readFileLineByLine AddToCorrespondedList"
def readFileLineByLine(fName):
  with open(fName, 'r', encoding="utf8") as f:
    content = f.readlines()
    content = [x.strip() for x in content]
    return content

"parseFromListToDict IfBothKeyAndValueIsPresentAccordingToRegEx"
def parseFromListToDict(lst):
  dct = dict()
  for item in lst:
    if item != '':
      key = findKey(item)
      value = findValue(item)
      dct[key] = value
  return dct

def addNewLineInXml(key, value, fName):
    strToAdd = '<string name=' + key + '">' + value + '</string>'

    with open(fName, 'a', encoding="utf8") as outfile:
        outfile.write("\n" + strToAdd)
        outfile.close()

lstEng = readFileLineByLine("strings_eng")
lstKa = readFileLineByLine("strings_ka")
lstRu = readFileLineByLine("strings_ru")

dctEng = parseFromListToDict(lstEng)
dctKa = parseFromListToDict(lstKa)
dctRu = parseFromListToDict(lstRu)

for key,value in dctEng.items():
    if key not in dctKa:
        addNewLineInXml(key, value, "strings_ka")
    if key not in dctRu:
        addNewLineInXml(key, value, "strings_ru")

